<?php
if (isset($_POST['signin'])) {

    require 'conn.php';
    $Email = $_POST['em'];
    $Password = $_POST['pw'];
    if (empty($Email) || empty($Password)) {
        header("Location: ../index.php?error=emptyfields");
        exit();
    } else {
        $sql = 'SELECT * FROM auth WHERE email= ?';
        $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: ../index.php?error=sqlerror");
            exit();

        } else {
            mysqli_stmt_bind_param($stmt, "s", $Email);
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
#to fetch row in associative array
            if ($row = mysqli_fetch_assoc($result)) {
 #to test password it returns a boolean compares between the database encrypted password and the signin password
                $pwtest = password_verify($Password, $row['upassword']);
                if ($pwtest == 1) {
#if password test pass start a session (every user has his own session
                    session_start();
#declare session variables and define it to row array variables and run session start into head file to globalize this vars
#into every file
                    $_SESSION['uid'] = $row['id'];
                    $_SESSION['uuid'] = $row['Uname'];
                    header("Location: ../index.php?login=success");
                    exit();

                } else {
                    header("Location: ../index.php?error=wrongpassword");

                    exit();
                }
            } else {
                header("Location: ../index.php?error=nouser");
                exit();
            }
        }
    }
} else {
    header("Location: ../index.php");
    exit();
}